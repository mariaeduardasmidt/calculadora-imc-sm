## Soluções Mobile
Atividade - Calculadora de IMC (Java)

```bash
Objetivo: Utilizar Java para criar uma Calculadora de IMC, 
realizando os cálculos e classificando os resultados.

Descrição: Aplicativo Mobile Android nativo, calculadora de IMC.
```
Desenvolvido por: Maria Eduarda Smidt da Silva e Carlos Daniel Pasquali.  
Engenharia da Computação - Universidade SATC
